/******************************************************************************
 * :: method nico_watch(url) -> String                                        *
 *                                                                            *
 * Takes a NicoVideo URL and returns a MMCafe URL, which you can use to watch *
 * the NicoVideo without gay signups.                                         *
 ******************************************************************************/
function nico_watch(url) {
	var base = "http://mmcafe.com/nico.html"
	  , smid = url && ("#sm" + url.match(/\/sm(\d+)/)[1])

	return base + smid
}


/******************************************************************************
 * :: method mdc_js_ref(obj) -> String                                        *
 *                                                                            *
 * Takes a JavaScript dot notated object and returns the MDC reference URL    *
 * for it. Alternatively it can also return the reference for JavaScript      *
 * statements (which are detected automatically).                             *
 ******************************************************************************/
function mdc_js_ref(obj) {
	var // List of words recognized as JavaScript statements
	    stmts = ["block", "break", "const", "continue", "do...while", "export"
	            ,"for", "for...in", "for each...in", "function", "if...else"
	            ,"import", "label", "let", "return", "switch", "throw"
	            ,"try...catch", "var", "while", "with"]

	    // Base URL of the javascript reference
	  , base_ref = "https://developer.mozilla.org/en/JavaScript/Reference/"
	
	if (stmts.indexOf(obj) != -1)
	    return base_ref + "Statements/" + obj
	else
	    return base_ref + "Global_Objects/" + obj.replace(".", "/")
}

/******************************************************************************
 * :: method prototype_search_api(obj) -> String                              *
 *                                                                            *
 * Returns a path to the Prototype API reference                              *
 ******************************************************************************/
var prototype_api = {};
let (p = get_file_in_path("~/.conkerorrc/prototype.json"), f) {
	f = read_text_file(p)
	if (f) prototype_api = JSON.parse(f)
}
function prototype_search_api(obj) {
	var path = prototype_api[obj]
	  , base = "http://api.prototypejs.org/"

	dumpln(base + (path || ""))
	return base + (path || "")
}


/******************************************************************************
 * :: method is_page_down(url) -> String                                      *
 *                                                                            *
 * Takes an URL and returns the URL for checking if a site is really down     *
 * using the `Down For Everyone Or Just Me` website.                          *
 ******************************************************************************/
function is_page_down(url) {
	var base = "http://downforeveryoneorjustme.com/"

	if (url) return base + url
	else     return "javascript:window.location.href='" + base
	                + window.location.href + "'"
}


// ----------------------------------------------------------------------------
// Videos
// ----------------------------------------------------------------------------
define_webjump("nico", nico_watch, $argument = "optional")


// ----------------------------------------------------------------------------
// Boorus
// ----------------------------------------------------------------------------
define_webjump("booru", "http://safebooru.org/index.php?page=post&s=list&tags=%s")



// ----------------------------------------------------------------------------
// Wikipedia
// ----------------------------------------------------------------------------
require("page-modes/wikipedia.js")

wikipedia_webjumps_format = "wp-%s"
define_wikipedia_webjumps("en", "pt")


// ----------------------------------------------------------------------------
// Languages
// ----------------------------------------------------------------------------
define_webjump("gt", "http://translate.google.com/translate_t#auto|en|%s")
define_webjump("gd", "http://www.google.com/dictionary?langpair=en|en&q=%s")
define_webjump("gde", "http://www.google.com/dictionary?langpair=de|en&q=%s")
define_webjump("gdp", "http://www.google.com/dictionary?langpair=pt|en&q=%s")
define_webjump("pt", "http://www.priberam.pt/DLPO/default.aspx?pal=%s")


// ----------------------------------------------------------------------------
// Programming
// ----------------------------------------------------------------------------
define_webjump("rfc", "http://www.ietf.org/rfc/rfc%s.txt")

/** Javascript **/
define_webjump("mdc", mdc_js_ref)
define_webjump("prototype", prototype_search_api)


// ----------------------------------------------------------------------------
// Network
// ----------------------------------------------------------------------------
define_webjump("down?", is_page_down
              ,$argument  = "optional"
              ,$completer = history_completer($use_history   = false
                                             ,$use_bookmarks = true ))


// ----------------------------------------------------------------------------
// Shortcuts
// ----------------------------------------------------------------------------
webjumps.g  = webjumps["google"]
webjumps.w  = webjumps["wp-en"]
webjumps.wd = webjumps["wiktionary"]


// ----------------------------------------------------------------------------
// Bookmarklets
// ----------------------------------------------------------------------------

/** Readability arc90 **/
define_webjump("readable", "javascript:(function(){readConvertLinksToFootnotes=true;readStyle='style-novel';readSize='size-medium';readMargin='margin-wide';_readability_script=document.createElement('script');_readability_script.type='text/javascript';_readability_script.src='http://lab.arc90.com/experiments/readability/js/readability.js?x='+(Math.random());document.documentElement.appendChild(_readability_script);_readability_css=document.createElement('link');_readability_css.rel='stylesheet';_readability_css.href='http://lab.arc90.com/experiments/readability/css/readability.css';_readability_css.type='text/css';_readability_css.media='all';document.documentElement.appendChild(_readability_css);_readability_print_css=document.createElement('link');_readability_print_css.rel='stylesheet';_readability_print_css.href='http://lab.arc90.com/experiments/readability/css/readability-print.css';_readability_print_css.media='print';_readability_print_css.type='text/css';document.getElementsByTagName('head')[0].appendChild(_readability_print_css);})();")