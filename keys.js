// ----------------------------------------------------------------------------
// Generic helpers
// ----------------------------------------------------------------------------
define_key(default_global_keymap, "C-x r", "rc-reload")

// Switch buffers by using the numeric keys
function def_switch_buf(key, buf_num) {
	define_key(default_global_keymap, key, function(I)
		switch_to_buffer(I.window
		                ,I.window.buffers.get_buffer(buf_num)))
}
for (let i = 0; i < 10; ++i) def_switch_buf("" + (i+1), i)


// ----------------------------------------------------------------------------
// Content buffer helpers
// ----------------------------------------------------------------------------
define_key(content_buffer_normal_keymap, "C-J", "firebug")
define_key(content_buffer_normal_keymap, "C-x f"
                                       , "follow-new-buffer-background")




