// ----------------------------------------------------------------------------
// Fixes load path
// ----------------------------------------------------------------------------
load_paths.unshift("chrome://conkeror-contrib/content/")


// ----------------------------------------------------------------------------
// Usually auto-explaining faggy configs
// ----------------------------------------------------------------------------
homepage                        = "http://en.wikipedia.org/wiki/Special:Random"
view_source_use_external_editor = true
xkcd_add_title                  = true
url_remoting_fn                 = load_url_in_new_buffer


// Don't use a new window for downloads
download_buffer_automatic_open_target = OPEN_NEW_BUFFER_BACKGROUND


// Use emacs to edit mai forms
editor_shell_command = "emacsclient -c"


// tab stuff
require("new-tabs.js")
require("clicks-in-new-buffer.js")

clicks_in_new_buffer_target = OPEN_NEW_BUFFER_BACKGROUND


// Display URL before blindly going into it
// TODO: It doesn't seem to resolve minified URLs, which bugs me freaking much,
//       so a hook to auto-expand minified URLs would be nice.
hints_display_url_panel = true


// Provides a nice function to reload the resource configurations
interactive("rc-reload", "Reload the configuration files."
           ,function() load_rc())

// ----------------------------------------------------------------------------
// Widget hooks
// ----------------------------------------------------------------------------
remove_hook("mode_line_hook", mode_line_adder(clock_widget))


// ----------------------------------------------------------------------------
// User preferences
// ----------------------------------------------------------------------------
user_pref("extensions.checkCompatibility", false)
user_pref("extensions.mozrepl.autoStart",  true)


// ----------------------------------------------------------------------------
// MozRepl
// ----------------------------------------------------------------------------
let (repl_init = get_home_directory()) {
	repl_init.appendRelativePath(".conkerorrc/mozrepl.js")
	session_pref("extensions.mozrepl.initUrl", make_uri(repl_init).spec)
}
