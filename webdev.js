// ----------------------------------------------------------------------------
// Includes firebug-lite
// ----------------------------------------------------------------------------
function firebug(I) {
	var doc = I.buffer.document
	  , elm = doc.createElement("script")

	elm.setAttribute("type",   "text/javascript")
	elm.setAttribute("src",    "https://getfirebug.com/firebug-lite.js")

	doc.body.appendChild(elm)
}
interactive("firebug", "Open Firebug-lite", firebug)


