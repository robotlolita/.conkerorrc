/**
 * Use MozRepl with Conkeror
 */
var conkeror = Cc["@conkeror.mozdev.org/application;1"].getService()
                                                       .wrappedJSObject

// Makes current window's properties easily accessible
repl.__defineGetter__("win"
                     ,function() conkeror.get_recent_conkeror_window())
repl.__defineGetter__("buf"
                     ,function() repl.win.buffers.current)
repl.__defineGetter__("dom"
                     ,function() repl.buf.document)

// Enters the conkeror context
this.enter(conkeror)
